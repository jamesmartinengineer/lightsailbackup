#Import-Module AWSPowerShell

# Created by James Martin (james.martin@engineer.com) 
# v1.0 - 19/1/2017 

# v1.1 - 9/7/2019 - Modified to run as serverless with local keys

#variables
$emailTo = "myemail@mailaddress.com"   #emails automation results to this address
$days = 31 #days to store snaps for

# When executing in Lambda the following variables will be predefined.
#   $LambdaInput - A PSObject that contains the Lambda function input data.
#   $LambdaContext - An Amazon.Lambda.Core.ILambdaContext object that contains information about the currently running Lambda environment.
 
# To include PowerShell modules with your Lambda function, like the AWSPowerShell.NetCore module, add a "#Requires" statement
# indicating the module and version.
 
#Requires -Modules @{ModuleName='AWSPowerShell.NetCore';ModuleVersion='3.3.343.0'}
#Import-Module AWSPowerShell.NetCore

Write-Host "Lightsail Backup will snapshot/backup Lightsail services discovered in us-east-1 for all VPCs on the keylist"
Write-Host "This Automation will backup all discovered Lightsail Instances for $days days"

#set date for automations which will be saved onto the snap files. Unless this automation starts at 11:59PM, all backups should represent the correct date.
$Current_Date = Get-Date -UFormat "%Y-%m-%d"

$results = ""
foreach ($key in $keylist)
{
	$results = "Lightsail instance backup details:`n"
	$place = $key.VPC
	Write-Host "Logging into $place"

	# Define AWS User/VPC Membership
	aws configure set aws_access_key_id $key.Key
	aws configure set aws_secret_access_key $key.Secret_Key
	aws configure set region "ap-southeast-2"
	
	#retrieve all of the local instance names
	$lightsail_instances = aws lightsail get-instances --output json --query 'instances[*].name'
	$lightsail_instances | Out-File "c:\awsautomation\file.json"
	$lightsail_instances=(Get-Content -Raw c:\awsautomation\file.json | ConvertFrom-Json)
	foreach ($lightsail_instance in $lightsail_instances)
	{
		#snap each lightsail instance found in VPC/Region
		$instance_name = $lightsail_instance
		$instance_snap_name = "autosnap-" + $instance_name + "-" +  $Current_Date 
		#$instance_snap_name = "autosnap-" + $instance_name + "-" +  "2016-06-01"
		write-host "Creating snap - $instance_snap_name - for $instance_name"
		$snap_details = aws lightsail create-instance-snapshot --instance-name $instance_name --instance-snapshot-name $instance_snap_name
		#$snap_details = aws lightsail create-instance-snapshot --instance-name "RestoredInstance" --instance-snapshot-name "RestoredInstance-snaptest"
		
		#do Verbose
		#write and reimport variable as JSON - this is a work around
		$snap_details | Out-File "c:\awsautomation\file.json"
		$snap_details=(Get-Content -Raw c:\awsautomation\file.json | ConvertFrom-Json)
		
		#expand some object variables to basic functions so write-host can handle them
		$toWrite1 = $snap_details.operations[0].operationDetails
		$toWrite2 = $snap_details.operations[0].resourceName
		$toWrite3 = $snap_details.operations[0].createdAt
		$toWrite4 = $snap_details.operations[0].status
		$toWrite5 = $key.VPC
		
		#write info to console and variable for emailing
		Write-host "$toWrite5 : Snapshot of $toWrite1 has $toWrite4 as $toWrite2 and started at $toWrite3"
		$results = $results + $toWrite5 + ":Snapshot of " + $toWrite1 + " has " + $toWrite4 + " as " + $toWrite2 + " and started at " + $toWrite3 + "`n"	
	}
	
	$results = $results + "`nLightsail snap maintenance details:`n"
	
	#Perform snap check and delete anything older than 31 days via snap name
	$lightsail_instance_snaps = aws lightsail get-instance-snapshots --query 'instanceSnapshots[*].name'
	$lightsail_instance_snaps | Out-File "c:\awsautomation\file.json"
	$lightsail_instance_snaps=(Get-Content -Raw c:\awsautomation\file.json | ConvertFrom-Json)
	#$lightsail_instance_snaps
	foreach ($lightsail_instance_snap in $lightsail_instance_snaps)
	{
		#extract and process date string into date variable
		$instance_name = $lightsail_instance_snap
		$instance_year = $instance_name.Substring($instance_name.length - 10, 4)
		$instance_day = $instance_name.Substring($instance_name.length - 2)
		$instance_month = $instance_name.Substring(($instance_name.length - 5),2)
		$instance_date = date($instance_day + "-" + $instance_month + "-" + $instance_year) -UFormat "%Y-%m-%d"
		#$instance_date = date($instance_date)
		#write-host "I think the snap date is $instance_date, current date $Current_Date"
		
		#compare the date relative to now
		$daydiff = NEW-TIMESPAN  $instance_date  $Current_Date 
		
		#delete if older than $days
		if($daydiff -ge $days)
		{
			#write-host "Im going to delete SNAP $instance_name as it has a $daydiff day diff"
			$snap_details = aws lightsail delete-instance-snapshot --instance-snapshot-name $instance_name
			$snap_details | Out-File "c:\awsautomation\file.json"
			$snap_details=(Get-Content -Raw c:\awsautomation\file.json | ConvertFrom-Json)
			#$snap_details
			
			#expand some object variables to basic functions so write-host can handle them
			$toWrite1 = $snap_details.operations.resourceName
			$toWrite2 = $snap_details.operations.createdAt
			$toWrite3 = $snap_details.operations.operationType
			$toWrite4 = $snap_details.operations.status
			$toWrite6 = $snap_details.operations.resourceType
			$toWrite5 = $key.VPC
			
			#write info to console and variable for emailing
			Write-host "$toWrite5 : $toWrite6 of $instance_name has $toWrite4 at $toWrite3"
			$results = $results + $toWrite5 + ":" + $toWrite6 + " of " + $instance_name + " has " + $toWrite4 + " at " + $toWrite3 + " as of " + $toWrite2 + " as it had a " + $daydiff + " day diff`n"	
		}
	}
	
	#Emails $results
	$emailFrom = "myautomatio@mymail.com" 
	$subject = "Lightsail Backup Results for " + $key.VPC + " on " + $Current_Date
	$smtpServer = "smtp.mailprovider.com"  
	$smtp = new-object Net.Mail.SmtpClient($smtpServer)  
    $smtp.Send($emailFrom, $emailTo, $subject, ($results))  
}

# post backup stuff

     